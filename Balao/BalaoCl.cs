﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Balao
{
    public class BalaoCl
    {
        #region Atributos

        private int _altura;

        private string _direccao;

        private string _cor;


        #endregion

        #region Propriedades


        public int Altura
        {
            get { return _altura; }
            set { _altura = value; }
        }

        public string Direccao
        {
            get { return _direccao; }
            set { _direccao = value; }
        }



        public string Cor
        {
            get { return _cor; }
            set { _cor = value; }
        }


        #endregion

        #region Construtores
        public BalaoCl() : this(0, " ", "") { }
        public BalaoCl(int _altura, string _direccao, string cor)
        {
            Altura = _altura;
            Direccao = _direccao;
            Cor = cor;
        }
        public BalaoCl(BalaoCl balaoCl)
        {
            Cor = balaoCl.Cor;
            Direccao = balaoCl.Direccao;
            Altura = balaoCl.Altura;

        }

        #endregion

        #region Metodos

        public int Subir(int valor)
        {

            Altura += valor;
            Direccao = "subir";
            return Altura;
        }

        public int Descer(int valor)
        {
            if (Altura > 0)
            {
                Altura -= valor;
                Direccao = "descer";
            }
            return Altura;


        }

        public string AlterarCor(string novaCor)
        {
            Cor = novaCor;
            return novaCor;
        }
        public override string ToString()
        {
            if (Altura == 0)
            {
                return String.Format("O Balão de cor {0}, está na altura {1}. ", Cor, Altura);
            }
            else
            {
                return String.Format("O Balão de cor {0},vai a {1} na altura {2}. ", Cor, Direccao, Altura);
            }

        }


        #endregion

    }
}
