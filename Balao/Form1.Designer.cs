﻿namespace Balao
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.ComboBoxCor = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.LabelStatus = new System.Windows.Forms.Label();
            this.ButtonSubir = new System.Windows.Forms.Button();
            this.ButtonDescer = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ComboBoxCor
            // 
            this.ComboBoxCor.FormattingEnabled = true;
            this.ComboBoxCor.Items.AddRange(new object[] {
            "Seleccione a cor do balão",
            "Azul",
            "Vermelha",
            "Amarela"});
            this.ComboBoxCor.Location = new System.Drawing.Point(166, 43);
            this.ComboBoxCor.Name = "ComboBoxCor";
            this.ComboBoxCor.Size = new System.Drawing.Size(121, 21);
            this.ComboBoxCor.TabIndex = 0;
            this.ComboBoxCor.SelectedIndexChanged += new System.EventHandler(this.ComboBoxCor_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(134, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Seleccione a cor do balão:";
            // 
            // LabelStatus
            // 
            this.LabelStatus.AutoSize = true;
            this.LabelStatus.Location = new System.Drawing.Point(82, 138);
            this.LabelStatus.Name = "LabelStatus";
            this.LabelStatus.Size = new System.Drawing.Size(0, 13);
            this.LabelStatus.TabIndex = 2;
            // 
            // ButtonSubir
            // 
            this.ButtonSubir.Location = new System.Drawing.Point(132, 104);
            this.ButtonSubir.Name = "ButtonSubir";
            this.ButtonSubir.Size = new System.Drawing.Size(75, 23);
            this.ButtonSubir.TabIndex = 3;
            this.ButtonSubir.Text = "Subir";
            this.ButtonSubir.UseVisualStyleBackColor = true;
            this.ButtonSubir.Click += new System.EventHandler(this.ButtonSubir_Click);
            // 
            // ButtonDescer
            // 
            this.ButtonDescer.Location = new System.Drawing.Point(132, 169);
            this.ButtonDescer.Name = "ButtonDescer";
            this.ButtonDescer.Size = new System.Drawing.Size(75, 23);
            this.ButtonDescer.TabIndex = 4;
            this.ButtonDescer.Text = "Descer";
            this.ButtonDescer.UseVisualStyleBackColor = true;
            this.ButtonDescer.Click += new System.EventHandler(this.ButtonDescer_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(331, 282);
            this.Controls.Add(this.ButtonDescer);
            this.Controls.Add(this.ButtonSubir);
            this.Controls.Add(this.LabelStatus);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ComboBoxCor);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox ComboBoxCor;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label LabelStatus;
        private System.Windows.Forms.Button ButtonSubir;
        private System.Windows.Forms.Button ButtonDescer;
    }
}

