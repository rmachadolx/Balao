﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Balao
{
    public partial class Form1 : Form
    {
        BalaoCl ballon = new BalaoCl();
        public Form1()
        {
            InitializeComponent();
            ButtonSubir.Enabled = false;
            ButtonDescer.Enabled = false;
            ComboBoxCor.SelectedIndex = 0;
        }

        private void ComboBoxCor_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ComboBoxCor.SelectedIndex != 0)
            {
                ballon.Cor = ComboBoxCor.SelectedItem.ToString();
                LabelStatus.Text = ballon.ToString();
                ButtonSubir.Enabled = true;
                ButtonDescer.Enabled = true;

            }


        }

        private void ButtonSubir_Click(object sender, EventArgs e)
        {
            ballon.Subir(1);
            LabelStatus.Text = ballon.ToString();
        }




        private void ButtonDescer_Click(object sender, EventArgs e)
        {

            ballon.Descer(1);
            LabelStatus.Text = ballon.ToString();

        }
    }
}
